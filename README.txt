How to use this module
======================

1. Add a theme or module preprocess_views_view
2. Add the following code where relevant:
   $vars['direct_link'] = theme_views_direct_link($vars['view']->views_direct_link['path'], $vars['view']->views_direct_link['query']);
3. Edit the relevant views.view.tpl.php and add where you see fit:
   print $direct_link;
